package com.simple.ping.application.executors;

import com.simple.ping.application.services.ConnectionService;

import java.util.List;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Executor {
    private static final int INITIAL_DELAY = 0;
    private final List<ConnectionService> connectionServices;
    private final String[] hosts;
    private final SingleRunnableFactory singleRunnableFactory;
    private final ScheduledExecutorService executorService;

    public Executor(SingleRunnableFactory singleRunnableFactory, List<ConnectionService> connectionServices, String[] hosts,
                    ScheduledExecutorService executorService) {
        this.singleRunnableFactory = singleRunnableFactory;
        this.connectionServices = connectionServices;
        this.hosts = hosts;
        this.executorService = executorService;
    }

    public void runConnectionChecks() {
        for (ConnectionService connectionService : connectionServices) {
            for (String host : this.hosts) {
                Thread connectionServiceThread = new Thread(() -> connectionService.run(host));
                executorService.scheduleAtFixedRate(singleRunnableFactory.singleRunnable(connectionServiceThread),
                                                    INITIAL_DELAY,
                                                    connectionService.getDelay(),
                                                    TimeUnit.MILLISECONDS);
            }
        }
    }
}
