package com.simple.ping.application.executors;

public interface SingleRunnableFactory {
    default Runnable singleRunnable(Thread connectionServiceThread) {
        return new SingleRunnable(connectionServiceThread);
    }
}
