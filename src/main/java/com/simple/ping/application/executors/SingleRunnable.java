package com.simple.ping.application.executors;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class SingleRunnable implements Runnable {

    private final ExecutorService executor = Executors.newSingleThreadExecutor();
    ;
    private Future<?> lastExecution;
    private final Runnable connectionService;

    protected SingleRunnable(Runnable connectionService) {
        this.connectionService = connectionService;
    }

    @Override
    public void run() {
        if (lastExecution != null && !lastExecution.isDone()) {
            return;
        }
        lastExecution = executor.submit(connectionService);
    }

}
