package com.simple.ping.application.configuration;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import static com.simple.ping.application.configuration.Configuration.OS.UNIX;
import static com.simple.ping.application.configuration.Configuration.OS.WIN;

public class Configuration {
    public static final String DEFAULT_CONFIG_PROPERTIES_PATH = "src/main/resources/default.properties";
    public static final String CONFIG_PROPERTIES_PATH = "config.properties";
    private static final Logger LOGGER = Logger.getLogger(Configuration.class.getName());
    private static final String PING_DELAY = "ping.delay";
    private static final String PING_HOSTS = "ping.hosts";
    private static final String TCP_DELAY = "tcp.delay";
    private static final String TCP_TIMEOUT = "tcp.timeout";
    private static final String TRACE_DELAY = "trace.delay";
    private static final String REPORT_URL = "report.url";
    private static final String HOST_SEPARATOR = ";";
    private static final String OS_NAME_PROPERTY = "os.name";
    private static final String WINDOWS_IDENTIFIER = "win";
    private static final String DEFAULT_LOG_CONFIG = "src/main/resources/logging.properties";

    private static Configuration CONFIGURATION;

    Properties properties = new Properties();

    public static Configuration getInstance(String configurationFile) {
        if (CONFIGURATION == null) {
            CONFIGURATION = new Configuration(configurationFile);
        }
        return CONFIGURATION;
    }

    protected Configuration(String externalConfiguration) {
        String file = new File(externalConfiguration).exists() ? externalConfiguration : DEFAULT_CONFIG_PROPERTIES_PATH;
        try (InputStream input = new FileInputStream(file)) {
            properties.load(input);
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
            throw new RuntimeException("Error while loading Configuration");
        }

        configLogging();
    }

    public Integer getPingDelayInMillis() {
        return Integer.parseInt(properties.getProperty(PING_DELAY));
    }

    public String[] getHosts() {
        return properties.getProperty(PING_HOSTS, "")
                         .split(HOST_SEPARATOR);
    }

    public Integer getTcpDelayInMillis() {
        return Integer.parseInt(properties.getProperty(TCP_DELAY));
    }

    public Integer getTcpTimeoutInMillis() {
        return Integer.parseInt(properties.getProperty(TCP_TIMEOUT));
    }

    public Integer getTraceDelayInMillis() {
        return Integer.parseInt(properties.getProperty(TRACE_DELAY));
    }

    public String getReportUrl() {
        return properties.getProperty(REPORT_URL);
    }

    public static OS getOS() {
        return System.getProperty(OS_NAME_PROPERTY)
                     .toLowerCase()
                     .contains(WINDOWS_IDENTIFIER) ? WIN : UNIX;
    }

    public enum OS {
        WIN,
        UNIX
    }

    private void configLogging() {
        String logConfig = properties.getProperty("log.config", DEFAULT_LOG_CONFIG);
        try (InputStream input = new FileInputStream(new File(logConfig))) {
            LogManager.getLogManager()
                      .readConfiguration(input);
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
            throw new RuntimeException("Error while loading Logging config");
        }
    }
}
