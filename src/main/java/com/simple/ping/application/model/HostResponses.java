package com.simple.ping.application.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class HostResponses implements Serializable {

    private String host;
    private List<Response> responses;

    public HostResponses(String host) {
        this.host = host;
        this.responses = new ArrayList<>();
    }

    public String getHost() {
        return host;
    }

    public List<Response> getResponses() {
        return responses;
    }

    public void addResponse(Response response) {
        this.responses.removeIf(r -> response.getType()
                                             .equals(r.getType()));
        this.responses.add(response);
    }
}
