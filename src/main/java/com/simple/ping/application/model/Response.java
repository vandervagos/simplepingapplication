package com.simple.ping.application.model;

import java.io.Serializable;
import java.time.LocalDateTime;

public class Response implements Serializable {

    private final boolean success;
    private final ConnectionType type;
    private final String response;
    private final String dateTime;

    public Response(boolean success, ConnectionType type, String response) {
        this.success = success;
        this.type = type;
        this.response = response;
        this.dateTime = LocalDateTime.now()
                                     .toString();
    }

    public String getResponse() {
        return response;
    }

    public boolean isSuccess() {
        return success;
    }

    public ConnectionType getType() {
        return type;
    }

    public String getDateTime() {
        return dateTime;
    }
}
