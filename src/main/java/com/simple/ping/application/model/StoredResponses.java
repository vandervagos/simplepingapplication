package com.simple.ping.application.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class StoredResponses implements Serializable {

    private List<HostResponses> hostResponses = new ArrayList<>();

    public HostResponses getResponses(String host) {
        Optional<HostResponses> responsesOpt = hostResponses.stream()
                                                            .filter(hr -> host.equals(hr.getHost()))
                                                            .findFirst();
        if (!responsesOpt.isPresent()) {
            HostResponses responses = new HostResponses(host);
            hostResponses.add(responses);
            return responses;
        }
        else {
            return responsesOpt.get();
        }
    }
}
