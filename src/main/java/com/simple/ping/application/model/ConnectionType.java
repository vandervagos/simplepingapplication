package com.simple.ping.application.model;

import java.io.Serializable;

public enum ConnectionType implements Serializable {

    ICMP_PING("icmp_ping"),
    TCP_PING("tcp_ping"),
    TRACE("trace");

    private String value;

    ConnectionType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
