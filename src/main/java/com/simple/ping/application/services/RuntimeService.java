package com.simple.ping.application.services;

import java.io.IOException;

public interface RuntimeService {

    default Process exec(String command) throws IOException {
        return Runtime.getRuntime()
                      .exec(command);
    }
}
