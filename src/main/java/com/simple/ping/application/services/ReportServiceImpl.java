package com.simple.ping.application.services;

import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ReportServiceImpl implements ReportService {

    private static final Logger LOGGER = Logger.getLogger(ReportServiceImpl.class.getName());
    private static final String METHOD = "POST";
    private final String url;

    public ReportServiceImpl(String url) {
        this.url = url;
    }

    @Override
    public HttpURLConnection sendData(String host, String jsonReport) {
        try {
            byte[] out = jsonReport.getBytes(StandardCharsets.UTF_8);
            int length = out.length;

            HttpURLConnection http = (HttpURLConnection) new URL(addProtocol(this.url)).openConnection();
            http.setRequestMethod(METHOD);
            http.setDoOutput(true);
            http.setFixedLengthStreamingMode(length);
            http.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            http.connect();
            try (OutputStream os = http.getOutputStream()) {
                os.write(out);
            }
            return http;
        } catch (Throwable t) {
            LOGGER.log(Level.SEVERE, t.getMessage(), t);
            return null;
        }
    }

    private String addProtocol(String url) {
        return !url.startsWith("http") ? "http://" + url : url;
    }
}
