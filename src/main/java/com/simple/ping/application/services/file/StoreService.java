package com.simple.ping.application.services.file;

import com.simple.ping.application.model.ConnectionType;
import com.simple.ping.application.model.HostResponses;
import com.simple.ping.application.model.Response;

import static com.simple.ping.application.model.ConnectionType.ICMP_PING;
import static com.simple.ping.application.model.ConnectionType.TCP_PING;
import static com.simple.ping.application.model.ConnectionType.TRACE;

public interface StoreService {
    String QUOTE = "\"";
    String COLON = ":";
    String COMMA = ",";

    void write(String host, Response response);

    HostResponses read(String host);

    default String toJson(HostResponses hostResponses) {
        return "{"
                .concat(QUOTE)
                .concat("host")
                .concat(QUOTE)
                .concat(COLON)
                .concat(QUOTE)
                .concat(hostResponses.getHost())
                .concat(QUOTE)
                .concat(COMMA)
                .concat(QUOTE)
                .concat(ICMP_PING.getValue())
                .concat(QUOTE)
                .concat(COLON)
                .concat(QUOTE)
                .concat(getResponseForType(hostResponses, ICMP_PING))
                .concat(QUOTE)
                .concat(COMMA)
                .concat(QUOTE)
                .concat(TCP_PING.getValue())
                .concat(QUOTE)
                .concat(COLON)
                .concat(QUOTE)
                .concat(getResponseForType(hostResponses, TCP_PING))
                .concat(QUOTE)
                .concat(COMMA)
                .concat(QUOTE)
                .concat(TRACE.getValue())
                .concat(QUOTE)
                .concat(COLON)
                .concat(QUOTE)
                .concat(getResponseForType(hostResponses, TRACE))
                .concat(QUOTE)
                .concat("}");
    }

    default String getResponseForType(HostResponses hostResponses, ConnectionType type) {
        return hostResponses
                .getResponses()
                .stream()
                .filter(r -> type.equals(r.getType()))
                .findFirst()
                .map(value -> value.getDateTime() + " \n" + value.getResponse())
                .orElse("");
    }
}
