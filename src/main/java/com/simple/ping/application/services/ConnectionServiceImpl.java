package com.simple.ping.application.services;

import com.simple.ping.application.model.Response;
import com.simple.ping.application.services.commands.Command;
import com.simple.ping.application.services.file.StoreReadWriteLock;

import java.util.concurrent.CompletableFuture;
import java.util.logging.Logger;

public class ConnectionServiceImpl implements ConnectionService {

    private static final Logger LOGGER = Logger.getLogger(ConnectionServiceImpl.class.getName());

    private final int delay;
    private final Command command;
    private final StoreReadWriteLock storeReadWriteLock;
    private final ReportService reportService;

    public ConnectionServiceImpl(int delay, Command command, StoreReadWriteLock storeReadWriteLock, ReportService reportService) {
        this.delay = delay;
        this.command = command;
        this.storeReadWriteLock = storeReadWriteLock;
        this.reportService = reportService;
    }

    @Override
    public void run(String host) {
        Response response = command.execute(host);
        storeReadWriteLock.write(host, response);
        if (!response.isSuccess()) {
            String jsonReport = storeReadWriteLock.read(host);
            CompletableFuture.runAsync(() -> reportService.sendData(host, jsonReport));
            LOGGER.warning(jsonReport);
        }
    }

    @Override
    public int getDelay() {
        return this.delay;
    }
}
