package com.simple.ping.application.services;

public interface ConnectionService {

    void run(String host);

    int getDelay();
}
