package com.simple.ping.application.services.file;

import com.simple.ping.application.model.HostResponses;
import com.simple.ping.application.model.Response;
import com.simple.ping.application.model.StoredResponses;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.util.logging.Level;
import java.util.logging.Logger;

public class StoreServiceImpl implements StoreService {

    private static final Logger LOGGER = Logger.getLogger(StoreServiceImpl.class.getName());
    private static final String RESPONSES_FILE = "responses.dat";
    private static StoreServiceImpl INSTANCE;
    private final String file;
    private StoredResponses storedResponses;

    private StoreServiceImpl(String file) {
        this.file = file;
        readIfExists();
    }

    public static StoreServiceImpl getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new StoreServiceImpl(RESPONSES_FILE);
        }
        return INSTANCE;
    }

    public static StoreServiceImpl getInstance(String file) {
        if (INSTANCE == null) {
            INSTANCE = new StoreServiceImpl(file);
        }
        return INSTANCE;
    }

    @Override
    public void write(String host, Response response) {
        try (FileOutputStream fileOutputStream = new FileOutputStream(this.file);
             ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream)) {
            storedResponses.getResponses(host)
                           .addResponse(response);
            objectOutputStream.writeObject(storedResponses);
        } catch (Throwable e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
    }

    @Override
    public HostResponses read(String host) {
        readIfExists();
        return this.storedResponses.getResponses(host);
    }

    private void readIfExists() {
        if (new File(this.file).exists()) {
            readFile();
        }
        else {
            this.storedResponses = new StoredResponses();
        }
    }

    private void readFile() {
        try (FileInputStream fileInputStream = new FileInputStream(this.file);
             ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream)) {
            this.storedResponses = (StoredResponses) objectInputStream.readObject();
        } catch (Throwable e) {
            cleanFile(e);
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
    }

    private void cleanFile(Throwable e) {
        //In case the file is corrupted, delete and re-try
        try {
            this.storedResponses = new StoredResponses();
            Files.deleteIfExists(new File(this.file).toPath());
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
    }
}
