package com.simple.ping.application.services.commands;

import com.simple.ping.application.configuration.Configuration;
import com.simple.ping.application.model.Response;
import com.simple.ping.application.services.RuntimeService;

import static com.simple.ping.application.configuration.Configuration.OS.WIN;
import static com.simple.ping.application.model.ConnectionType.TRACE;

public class TraceRouteCommand implements Command {
    static final String TRACE_CMD_WIN = "tracert ";
    static final String TRACE_CMD_UNIX = "traceroute ";

    private final RuntimeService runtimeService;

    public TraceRouteCommand(RuntimeService runtimeService) {
        this.runtimeService = runtimeService;
    }

    @Override
    public Response execute(String host) {
        return executeRuntimeCommand(runtimeService, getTraceCommand(Configuration.getOS()), host, TRACE);
    }

    String getTraceCommand(Configuration.OS os) {
        return WIN.equals(os) ? TRACE_CMD_WIN : TRACE_CMD_UNIX;
    }
}
