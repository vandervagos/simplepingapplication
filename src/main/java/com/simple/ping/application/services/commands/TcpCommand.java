package com.simple.ping.application.services.commands;

import com.simple.ping.application.model.Response;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.simple.ping.application.model.ConnectionType.TCP_PING;

public class TcpCommand implements Command {
    private static final Logger LOGGER = Logger.getLogger(PingCommand.class.getName());
    private static final String METHOD = "GET";
    public static final String SPACE = " ";

    private final int timeout;

    public TcpCommand(int timeout) {
        this.timeout = timeout;
    }

    @Override
    public Response execute(String host) {
        try {
            String url = addProtocol(host);
            HttpURLConnection http = (HttpURLConnection) new URL(url).openConnection();
            http.setRequestMethod(METHOD);
            http.setConnectTimeout(timeout);
            http.setReadTimeout(timeout);
            long start = System.currentTimeMillis();
            http.connect();
            long end = System.currentTimeMillis();

            int responseCode = http.getResponseCode();
            boolean isSuccessful = responseCode == 200;

            return new Response(isSuccessful, TCP_PING, getResponse(url, start, end, responseCode));
        } catch (Throwable t) {
            LOGGER.log(Level.SEVERE, t.getMessage(), t);
            return new Response(false, TCP_PING, t.getMessage());
        }
    }

    public String getResponse(String url, long start, long end, int responseCode) {
        return "URL: "
                .concat(SPACE)
                .concat(url)
                .concat(SPACE)
                .concat("response time: ")
                .concat(end - start + "ms")
                .concat(SPACE)
                .concat("code")
                .concat(String.valueOf(responseCode));
    }

    private String addProtocol(String url) {
        return !url.startsWith("http") ? "http://" + url : url;
    }
}
