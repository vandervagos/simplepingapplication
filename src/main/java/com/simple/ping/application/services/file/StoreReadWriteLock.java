package com.simple.ping.application.services.file;

import com.simple.ping.application.model.Response;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class StoreReadWriteLock {
    private static StoreReadWriteLock INSTANCE;
    private final Lock readLock;
    private final Lock writeLock;
    private final StoreService storeService;

    public static StoreReadWriteLock getInstance(StoreService storeService) {
        if (INSTANCE == null) {
            INSTANCE = new StoreReadWriteLock(storeService);
        }
        return INSTANCE;
    }

    protected StoreReadWriteLock(StoreService storeService) {
        this(new ReentrantReadWriteLock(), storeService);
    }

    public StoreReadWriteLock(ReadWriteLock lock, StoreService storeService) {
        this.readLock = lock.readLock();
        this.writeLock = lock.writeLock();
        this.storeService = storeService;
    }

    public String read(String host) {
        readLock.lock();
        try {
            return this.storeService.toJson(this.storeService.read(host));
        } finally {
            readLock.unlock();
        }
    }

    public void write(String host, Response response) {
        writeLock.lock();
        try {
            this.storeService.write(host, response);
        } finally {
            writeLock.unlock();
        }
    }
}