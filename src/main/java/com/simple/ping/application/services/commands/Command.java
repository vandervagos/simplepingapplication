package com.simple.ping.application.services.commands;

import com.simple.ping.application.model.ConnectionType;
import com.simple.ping.application.model.Response;
import com.simple.ping.application.services.RuntimeService;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.util.stream.Collectors.joining;

public interface Command {

    Logger LOGGER = Logger.getLogger(PingCommand.class.getName());

    Response execute(String host);

    default Response executeRuntimeCommand(RuntimeService runtimeService, String command, String host, ConnectionType connectionType) {
        try {
            Process p = runtimeService.exec(command + host);

            String response = new BufferedReader(new InputStreamReader(p.getInputStream(), StandardCharsets.UTF_8))
                    .lines()
                    .collect(joining("\n"));
            p.waitFor();
            p.destroy();
            return new Response(p.exitValue() == 0, connectionType, response);
        } catch (Throwable e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            return new Response(false, connectionType, e.getMessage());
        }
    }
}
