package com.simple.ping.application.services.commands;

import com.simple.ping.application.configuration.Configuration;
import com.simple.ping.application.model.Response;
import com.simple.ping.application.services.RuntimeService;

import static com.simple.ping.application.configuration.Configuration.OS.WIN;
import static com.simple.ping.application.model.ConnectionType.ICMP_PING;

public class PingCommand implements Command {
    static final String PING_CMD_WIN = "ping -n 5 ";
    static final String PING_CMD_UNIX = "ping -c 5 ";

    private final RuntimeService runtimeService;

    public PingCommand(RuntimeService runtimeService) {
        this.runtimeService = runtimeService;
    }

    public Response execute(String host) {
        return executeRuntimeCommand(runtimeService, getPingCommand(Configuration.getOS()), host, ICMP_PING);
    }

    String getPingCommand(Configuration.OS os) {
        return WIN.equals(os) ? PING_CMD_WIN : PING_CMD_UNIX;
    }
}
