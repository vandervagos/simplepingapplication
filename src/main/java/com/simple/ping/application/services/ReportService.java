package com.simple.ping.application.services;

import java.net.HttpURLConnection;

public interface ReportService {

    HttpURLConnection sendData(String host, String jsonReport);
}
