package com.simple.ping.application;

import com.simple.ping.application.configuration.Configuration;
import com.simple.ping.application.executors.Executor;
import com.simple.ping.application.executors.SingleRunnableFactory;
import com.simple.ping.application.executors.SingleRunnableFactoryImpl;
import com.simple.ping.application.services.ConnectionService;
import com.simple.ping.application.services.ConnectionServiceImpl;
import com.simple.ping.application.services.ReportService;
import com.simple.ping.application.services.ReportServiceImpl;
import com.simple.ping.application.services.RuntimeService;
import com.simple.ping.application.services.RuntimeServiceImpl;
import com.simple.ping.application.services.commands.PingCommand;
import com.simple.ping.application.services.commands.TcpCommand;
import com.simple.ping.application.services.commands.TraceRouteCommand;
import com.simple.ping.application.services.file.StoreReadWriteLock;
import com.simple.ping.application.services.file.StoreService;
import com.simple.ping.application.services.file.StoreServiceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ScheduledThreadPoolExecutor;

public class Application {

    public static final int NUMBER_OF_CONNECTION_SERVICES = 3;

    public static void main(String[] args) {
        Configuration configuration = Configuration.getInstance(Configuration.CONFIG_PROPERTIES_PATH);
        List<ConnectionService> connectionServices = initConnectionServices(configuration);

        String[] hosts = configuration.getHosts();
        SingleRunnableFactory singleRunnableFactory = new SingleRunnableFactoryImpl();
        ScheduledThreadPoolExecutor executorService = new ScheduledThreadPoolExecutor(hosts.length * NUMBER_OF_CONNECTION_SERVICES);
        executorService.setRemoveOnCancelPolicy(true);

        Executor executor = new Executor(singleRunnableFactory, connectionServices, hosts, executorService);
        executor.runConnectionChecks();
    }

    public static List<ConnectionService> initConnectionServices(Configuration configuration) {
        RuntimeService runtimeService = new RuntimeServiceImpl();
        StoreService storeService = StoreServiceImpl.getInstance();
        StoreReadWriteLock storeReadWriteLock = StoreReadWriteLock.getInstance(storeService);
        ReportService reportService = new ReportServiceImpl(configuration.getReportUrl());
        ConnectionService pingService = new ConnectionServiceImpl(configuration.getPingDelayInMillis(), new PingCommand(runtimeService), storeReadWriteLock, reportService);
        ConnectionService tcpService = new ConnectionServiceImpl(configuration.getTcpDelayInMillis(),
                                                                 new TcpCommand(configuration.getTcpTimeoutInMillis()),
                                                                 storeReadWriteLock,
                                                                 reportService);
        ConnectionService traceService = new ConnectionServiceImpl(configuration.getTraceDelayInMillis(), new TraceRouteCommand(runtimeService), storeReadWriteLock,
                                                                   reportService);

        List<ConnectionService> connectionServices = new ArrayList<>();
        connectionServices.add(pingService);
        connectionServices.add(tcpService);
        connectionServices.add(traceService);
        return connectionServices;
    }
}
