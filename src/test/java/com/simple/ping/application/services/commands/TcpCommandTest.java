package com.simple.ping.application.services.commands;

import com.simple.ping.application.model.Response;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class TcpCommandTest {

    TcpCommand unit;

    @Test
    public void shouldReturnSuccessIfHostIsValid() {
        //GIVEN
        unit = new TcpCommand(5000);

        //WHEN
        Response response = unit.execute("https://postman-echo.com/get");

        //THEN
        assertNotNull(response);
        assertTrue(response.isSuccess());
    }

    @Test
    public void shouldReturnUnsuccessfulIf4XX() {
        //GIVEN
        unit = new TcpCommand(5000);

        //WHEN
        Response response = unit.execute("https://postman-echo.com/test");

        //THEN
        assertNotNull(response);
        assertFalse(response.isSuccess());
    }

    @Test
    public void shouldReturnUnsuccessfulIf5XX() {
        //GIVEN
        unit = new TcpCommand(5000);

        //WHEN
        Response response = unit.execute("http://127.0.0.1");

        //THEN
        assertNotNull(response);
        assertFalse(response.isSuccess());
    }

    @Test
    public void shouldReturnUnsuccessfulIfRequestTimeOut() {
        //GIVEN
        unit = new TcpCommand(1);

        //WHEN
        Response response = unit.execute("https://postman-echo.com");

        //THEN
        assertNotNull(response);
        assertFalse(response.isSuccess());
    }
}