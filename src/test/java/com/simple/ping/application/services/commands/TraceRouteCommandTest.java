package com.simple.ping.application.services.commands;

import com.simple.ping.application.configuration.Configuration;
import com.simple.ping.application.model.Response;
import com.simple.ping.application.services.RuntimeService;
import com.simple.ping.application.services.RuntimeServiceImpl;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class TraceRouteCommandTest {

    TraceRouteCommand unit;

    @Test
    public void shouldTrace() {
        String host = "127.0.0.1";
        unit = new TraceRouteCommand(new RuntimeServiceImpl());
        Response response = unit.execute(host);
        assertNotNull(response);
        assertTrue(response.isSuccess());
    }

    @Test
    public void shouldTraceNonExistentHost() {
        String host = "www.qwefghads.com";
        unit = new TraceRouteCommand(new RuntimeServiceImpl());
        Response response = unit.execute(host);
        assertNotNull(response);
        assertFalse(response.isSuccess());
    }

    @Test
    public void shouldReturnTraceForWindows_ifOsIsWIN() {
        unit = new TraceRouteCommand(new RuntimeServiceImpl());
        String traceCommand = unit.getTraceCommand(Configuration.OS.WIN);
        assertEquals(TraceRouteCommand.TRACE_CMD_WIN, traceCommand);
    }

    @Test
    public void shouldReturnTraceForWindows_ifOsIsUNIX() {
        unit = new TraceRouteCommand(new RuntimeServiceImpl());
        String traceCommand = unit.getTraceCommand(Configuration.OS.UNIX);
        assertEquals(TraceRouteCommand.TRACE_CMD_UNIX, traceCommand);
    }

    @Test
    public void shouldCatchExceptions() {
        String host = "127.0.0.1";
        unit = new TraceRouteCommand(new RuntimeService() {
            @Override
            public Process exec(String command) {
                throw new RuntimeException("Random error");
            }
        });
        Response response = unit.execute(host);
        assertNotNull(response);
        assertFalse(response.isSuccess());
    }
}