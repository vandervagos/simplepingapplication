package com.simple.ping.application.services.file;

import com.simple.ping.application.model.ConnectionType;
import com.simple.ping.application.model.HostResponses;
import com.simple.ping.application.model.Response;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

import static com.simple.ping.application.model.ConnectionType.ICMP_PING;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class StoreServiceTest {

    private String file = "test.dat";
    private StoreServiceImpl unit = StoreServiceImpl.getInstance(file);

    @Before
    public void before() throws IOException {
        deleteFile();
    }

    @After
    public void after() throws IOException {
        deleteFile();
    }

    private void deleteFile() throws IOException {
        Files.deleteIfExists(new File(file).toPath());
    }

    @Test
    public void shouldWriteAndRead() throws IOException {
        //GIVEN
        String host = "host";
        String commandResponse = "command response";
        Response response = new Response(true, ICMP_PING, commandResponse);

        //WHEN
        unit.write(host, response);
        HostResponses hostResponses = unit.read(host);

        //THEN
        assertNotNull(hostResponses);
        List<Response> responses = hostResponses.getResponses();
        assertEquals(1, responses.size());
        Response actualResponse = responses.get(0);
        assertTrue(actualResponse.isSuccess());
        assertEquals(ICMP_PING, actualResponse.getType());
        assertEquals(commandResponse, actualResponse.getResponse());

    }

    @Test
    public void shouldCreateFileIfDoesNotExist() throws IOException {
        //GIVEN
        String host = "host";

        //WHEN
        HostResponses hostResponses = unit.read(host);

        //THEN
        assertNotNull(hostResponses);
        List<Response> responses = hostResponses.getResponses();
        assertEquals(0, responses.size());
    }

    @Test
    public void shouldConvertToJson() {
        //GIVEN
        String host = "host";
        HostResponses hostResponses = new HostResponses(host);
        String message1 = "message1";
        String message2 = "message2";
        String message3 = "message3";
        Response response1 = new Response(true, ConnectionType.ICMP_PING, message1);
        Response response2 = new Response(false, ConnectionType.TCP_PING, message2);
        Response response3 = new Response(false, ConnectionType.TRACE, message3);
        String expected = "{\"host\":\"host\",\"icmp_ping\":\"" + response1.getDateTime() + " \nmessage1\",\"tcp_ping\":\"" + response2.getDateTime() + " \nmessage2\"," +
                          "\"trace\":" + "\"" + response3.getDateTime() + " \n" + "message3\"}";
        hostResponses.addResponse(response1);
        hostResponses.addResponse(response2);
        hostResponses.addResponse(response3);

        //WHEN
        String actual = unit.toJson(hostResponses);

        //THEN
        assertEquals(expected, actual);
    }

    @Test
    public void shouldConvertToJsonIfResponseMissing() {
        //GIVEN
        String host = "host";
        HostResponses hostResponses = new HostResponses(host);
        String message1 = "message1";
        String message3 = "message3";
        Response response1 = new Response(true, ConnectionType.ICMP_PING, message1);
        Response response3 = new Response(false, ConnectionType.TRACE, message3);
        hostResponses.addResponse(response1);
        hostResponses.addResponse(response3);
        String expected = "{\"host\":\"host\",\"icmp_ping\":\"" + response1.getDateTime() + " \nmessage1\",\"tcp_ping\":\"\",\"trace\":\"" + response3.getDateTime() +
                          " \nmessage3\"}";

        //WHEN
        String actual = unit.toJson(hostResponses);

        //THEN
        assertEquals(expected, actual);
    }
}