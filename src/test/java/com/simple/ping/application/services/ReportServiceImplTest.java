package com.simple.ping.application.services;

import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class ReportServiceImplTest {

    private ReportServiceImpl unit;
    private String expectedJson = "{\"host\":\"host\",\"icmp_ping\":\"message1\",\"tcp_ping\":\"message2\",\"trace\":\"message3\"}";

    @Test
    public void shouldPostReport() throws IOException {
        unit = new ReportServiceImpl("https://postman-echo.com/post");

        HttpURLConnection connection = unit.sendData("host", expectedJson);
        assertEquals(200, connection.getResponseCode());
        String text = new BufferedReader(new InputStreamReader(connection.getInputStream(), StandardCharsets.UTF_8))
                .lines()
                .collect(Collectors.joining("\n"));
        assertTrue(text.contains("\"json\":" + expectedJson));
    }

    @Test
    public void shouldReturnNullIfUrlInvalid() throws IOException {
        unit = new ReportServiceImpl("!@#$");

        HttpURLConnection connection = unit.sendData("host", expectedJson);
        assertNull(connection);
    }

    @Test
    public void shouldNotReturn200lIfPostNotPossible() throws IOException {
        unit = new ReportServiceImpl("www.google.com");

        HttpURLConnection connection = unit.sendData("host", expectedJson);
        assertNotEquals(200, connection.getResponseCode());
    }
}