package com.simple.ping.application.services;

import com.simple.ping.application.model.ConnectionType;
import com.simple.ping.application.model.HostResponses;
import com.simple.ping.application.model.Response;
import com.simple.ping.application.services.commands.Command;
import com.simple.ping.application.services.file.StoreReadWriteLock;
import com.simple.ping.application.services.file.StoreService;
import org.junit.Test;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ConnectionServiceImplTest {

    private ConnectionServiceImpl unit;

    @Test
    public void shouldReturnDelay() {
        int delay = 5;
        unit = new ConnectionServiceImpl(delay, null, null, null);

        assertEquals(delay, unit.getDelay());
    }

    @Test
    public void shouldRunCommandAndStoreIfResponseIsSuccessful() {
        //GIVEN
        AtomicBoolean commandExecuted = new AtomicBoolean(false);
        AtomicBoolean writeExecuted = new AtomicBoolean(false);

        Command command = (host) -> {
            commandExecuted.set(true);
            return new Response(true, ConnectionType.ICMP_PING, "");
        };

        StoreService storeService = new StoreService() {
            @Override
            public void write(String host, Response response) {
                writeExecuted.set(true);
            }

            @Override
            public HostResponses read(String host) {
                return null;
            }
        };
        StoreReadWriteLock storeReadWriteLock = StoreReadWriteLock.getInstance(storeService);
        unit = new ConnectionServiceImpl(5, command, storeReadWriteLock, null);

        //WHEN
        unit.run("host");

        //THEN
        assertTrue(commandExecuted.get());
        assertTrue(writeExecuted.get());
    }

    @Test
    public void shouldRunCommandStoreReportAndIfResponseIsSuccessful() throws InterruptedException {
        //GIVEN
        AtomicBoolean commandExecuted = new AtomicBoolean(false);
        AtomicBoolean writeExecuted = new AtomicBoolean(false);
        AtomicBoolean reportExecuted = new AtomicBoolean(false);

        Command command = (host) -> {
            commandExecuted.set(true);
            return new Response(false, ConnectionType.ICMP_PING, "");
        };

        StoreService storeService = new StoreService() {
            @Override
            public void write(String host, Response response) {
                writeExecuted.set(true);
            }

            @Override
            public HostResponses read(String host) {
                return null;
            }

            @Override
            public String toJson(HostResponses hostResponses) {
                return "";
            }

            @Override
            public String getResponseForType(HostResponses hostResponses, ConnectionType type) {
                return "";
            }
        };
        ReportService reportService = (host, jsonReport) -> {
            reportExecuted.set(true);
            return null;
        };
        StoreReadWriteLock storeReadWriteLock = new StoreReadWriteLock(storeService) {
            @Override
            public String read(String host) {
                return null;
            }

            @Override
            public void write(String host, Response response) {
                storeService.write(host, response);
            }
        };
        unit = new ConnectionServiceImpl(5, command, storeReadWriteLock, reportService);

        //WHEN

        unit.run("host");

        TimeUnit.MILLISECONDS.sleep(200);
        assertTrue(commandExecuted.get());
        assertTrue(writeExecuted.get());
        assertTrue(reportExecuted.get());
    }
}