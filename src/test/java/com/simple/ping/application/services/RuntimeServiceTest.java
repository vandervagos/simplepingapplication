package com.simple.ping.application.services;

import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertNotNull;

public class RuntimeServiceTest {

    RuntimeService unit = new RuntimeService() {};

    @Test
    public void shouldRunBasicCommandWithNoExceptions() throws IOException {
        Process process = unit.exec("cd /");
        process.destroy();
        assertNotNull(process);
    }

}