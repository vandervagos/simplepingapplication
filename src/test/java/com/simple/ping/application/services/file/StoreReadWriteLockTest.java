package com.simple.ping.application.services.file;

import com.simple.ping.application.model.HostResponses;
import com.simple.ping.application.model.Response;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import static com.simple.ping.application.model.ConnectionType.ICMP_PING;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class StoreReadWriteLockTest {

    StoreReadWriteLock unit;

    @Test
    public void shouldBlockReadIfWriting() throws InterruptedException, ExecutionException {

        AtomicInteger check = new AtomicInteger();

        StoreService storeService = new StoreService() {
            @Override
            public void write(String host, Response response) {
                try {
                    TimeUnit.MILLISECONDS.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                check.incrementAndGet();
            }

            @Override
            public HostResponses read(String host) {
                return check.get() == 1 ? new HostResponses("") : null;
            }
        };
        unit = new StoreReadWriteLock(storeService);
        ExecutorService executor = Executors.newFixedThreadPool(2);

        //WHEN
        Future future1 = executor.submit(() -> unit.write(null, null));
        TimeUnit.MILLISECONDS.sleep(50);
        Future<String> future2 = executor.submit(() -> unit.read(null));

        //THEN
        while (!future1.isDone() || !future2.isDone()) {
        }

        assertNotNull(future2.get());
    }

    @Test
    public void shouldReadWithMultipleThreads() throws InterruptedException, IOException {
        //GIVEN
        String file = "test.dat";
        Files.deleteIfExists(new File(file).toPath());
        unit = new StoreReadWriteLock(StoreServiceImpl.getInstance(file));
        String host = "host";
        String commandResponse = "command response";
        Response response = new Response(true, ICMP_PING, commandResponse);
        final String time = response.getDateTime();
        unit.write(host, response);
        TimeUnit.MILLISECONDS.sleep(100);

        //WHEN
        int maxThreads = 10;
        List<Callable<Boolean>> threads = new ArrayList<>(maxThreads);
        for (int i = 0; i < maxThreads; i++) {
            threads.add(() -> {
                //THEN
                String json = unit.read(host);
                System.out.println(json);
                String expected = "{\"host\":\"host\",\"icmp_ping\":\"" + time + " \n" +
                                  "command response\",\"tcp_ping\":\"\",\"trace\":\"\"}";
                return expected.equals(json);
            });
        }
        ExecutorService executor = Executors.newFixedThreadPool(maxThreads);
        List<Future<Boolean>> futures = executor.invokeAll(threads);
        boolean result = futures.stream()
                                .allMatch(booleanFuture -> {
                                    try {
                                        return booleanFuture.get();
                                    } catch (InterruptedException | ExecutionException e) {
                                        e.printStackTrace();
                                    }
                                    return false;
                                });

        assertTrue(result);
        Files.deleteIfExists(new File(file).toPath());
    }

    @Test
    public void shouldWriteSequentially() throws InterruptedException, ExecutionException {

        StoreService storeService = new StoreService() {
            @Override
            public void write(String host, Response response) {
                try {
                    TimeUnit.MILLISECONDS.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public HostResponses read(String host) {
                return null;
            }
        };
        unit = new StoreReadWriteLock(storeService);
        ExecutorService executor = Executors.newFixedThreadPool(2);

        //WHEN
        Future future1 = executor.submit(() -> unit.write(null, null));
        TimeUnit.MILLISECONDS.sleep(10);
        Future future2 = executor.submit(() -> unit.write(null, null));

        //THEN
        while (!future1.isDone()) {
            assertFalse(future2.isDone());
        }
        TimeUnit.MILLISECONDS.sleep(100);

        assertTrue(future2.isDone());
    }

}