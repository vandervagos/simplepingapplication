package com.simple.ping.application.services.commands;

import com.simple.ping.application.configuration.Configuration;
import com.simple.ping.application.model.Response;
import com.simple.ping.application.services.RuntimeService;
import com.simple.ping.application.services.RuntimeServiceImpl;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class PingCommandTest {

    PingCommand unit;

    @Test
    public void shouldPing() {
        String host = "127.0.0.1";
        unit = new PingCommand(new RuntimeServiceImpl());
        Response response = unit.execute(host);
        assertNotNull(response);
        assertTrue(response.isSuccess());
    }

    @Test
    public void shouldPingNonExistentHost() {
        String host = "not-exist.co.ok";
        unit = new PingCommand(new RuntimeServiceImpl());
        Response response = unit.execute(host);
        assertNotNull(response);
        assertFalse(response.isSuccess());
    }

    @Test
    public void shouldReturnPingForWindows_ifOsIsWIN() {
        unit = new PingCommand(new RuntimeServiceImpl());
        String pingCommand = unit.getPingCommand(Configuration.OS.WIN);
        assertEquals(PingCommand.PING_CMD_WIN, pingCommand);
    }

    @Test
    public void shouldReturnPingForWindows_ifOsIsUNIX() {
        unit = new PingCommand(new RuntimeServiceImpl());
        String pingCommand = unit.getPingCommand(Configuration.OS.UNIX);
        assertEquals(PingCommand.PING_CMD_UNIX, pingCommand);
    }

    @Test
    public void shouldCatchExceptions() {
        String host = "127.0.0.1";
        unit = new PingCommand(new RuntimeService() {
            @Override
            public Process exec(String command) {
                throw new RuntimeException("Random error");
            }
        });
        Response response = unit.execute(host);
        assertNotNull(response);
        assertFalse(response.isSuccess());
    }
}