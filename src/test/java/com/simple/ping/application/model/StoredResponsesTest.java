package com.simple.ping.application.model;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class StoredResponsesTest {

    private StoredResponses unit;

    @Test
    public void shouldReturnResponsesIfHostDoesNotExist() {
        unit = new StoredResponses();

        String expectedHost = "host";
        HostResponses hostResponses = unit.getResponses(expectedHost);
        assertNotNull(hostResponses);
        assertEquals(expectedHost, hostResponses.getHost());
        assertEquals(0,
                     hostResponses.getResponses()
                                  .size());
    }

    @Test
    public void shouldReturnResponsesPerHost() {
        String host1 = "host1";
        String host2 = "host2";
        unit = new StoredResponses();
        HostResponses hostResponses1 = unit.getResponses(host1);
        hostResponses1.addResponse(new Response(true, ConnectionType.ICMP_PING, ""));
        hostResponses1.addResponse(new Response(true, ConnectionType.TCP_PING, ""));

        HostResponses hostResponses2 = unit.getResponses(host2);
        hostResponses2.addResponse(new Response(true, ConnectionType.ICMP_PING, ""));

        HostResponses hostResponses = unit.getResponses(host1);
        assertNotNull(hostResponses);
        assertEquals(2,
                     hostResponses.getResponses()
                                  .size());

        hostResponses = unit.getResponses(host2);
        assertNotNull(hostResponses);
        assertEquals(1,
                     hostResponses.getResponses()
                                  .size());
    }

}