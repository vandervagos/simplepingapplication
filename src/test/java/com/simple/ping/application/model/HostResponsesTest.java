package com.simple.ping.application.model;

import org.junit.Test;

import static com.simple.ping.application.model.ConnectionType.ICMP_PING;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class HostResponsesTest {

    HostResponses unit;

    @Test
    public void shouldReplaceTypeIfExists() {
        //GIVEN
        String expectedResponse = "expectedResponse";
        unit = new HostResponses("host");
        unit.addResponse(new Response(false, ICMP_PING, "oldResponse"));

        //WHEN
        unit.addResponse(new Response(true, ICMP_PING, expectedResponse));

        //THEN
        assertEquals(1,
                     unit.getResponses()
                         .size());
        Response response = unit.getResponses()
                                .get(0);
        assertTrue(response.isSuccess());
        assertEquals(ICMP_PING, response.getType());
        assertEquals(expectedResponse, response.getResponse());

    }

}