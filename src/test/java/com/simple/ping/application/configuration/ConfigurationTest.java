package com.simple.ping.application.configuration;

import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.util.Properties;

import static com.simple.ping.application.configuration.Configuration.DEFAULT_CONFIG_PROPERTIES_PATH;
import static java.util.logging.LogManager.getLogManager;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class ConfigurationTest {

    Configuration unit = Configuration.getInstance(DEFAULT_CONFIG_PROPERTIES_PATH);

    @Test
    public void shouldLoadDefaultConfigurationAndLoggingFile() {
        //GIVEN Configuration

        //THEN
        assertNotNull(unit);
        assertEquals("simple_ping_application.log", getLogManager().getProperty("java.util.logging.FileHandler.pattern"));
    }

    @Test
    public void shouldLoadExternalConfigurationFile() throws IOException {
        //GIVEN
        File file = new File("test.properties");
        Files.deleteIfExists(file.toPath());
        Properties prop = new Properties();
        prop.setProperty("ping.delay", "1234");
        prop.store(new ObjectOutputStream(new FileOutputStream(file)), "");

        //WHEN
        Configuration configuration = new Configuration("test.properties");

        //THEN
        assertEquals(1234,
                     configuration.getPingDelayInMillis()
                                  .intValue());
        Files.deleteIfExists(file.toPath());
    }

    @Test
    public void pingDelayShouldNotBeNull() {
        Integer pingDelay = unit.getPingDelayInMillis();
        assertNotNull(pingDelay);
    }

    @Test
    public void pingDelayShouldBeIntegerGreaterThanZero() {
        Integer pingDelay = unit.getPingDelayInMillis();
        assertTrue(pingDelay > 0);
    }

    @Test
    public void hostsShouldContainAtLeastOne() {
        String[] pingHosts = unit.getHosts();
        assertNotNull(pingHosts);
        assertTrue(pingHosts.length > 0);
    }

    @Test
    public void shouldDetermineOS() {
        Configuration.OS os = Configuration.getOS();
        assertNotNull(os);
    }

    @Test
    public void getTcpDelayInMillis() {
        Integer tcpDelay = unit.getTcpDelayInMillis();
        assertNotNull(tcpDelay);
    }

    @Test
    public void getTcpTimeoutInMillis() {
        Integer tcpTimeoutInMillis = unit.getTcpTimeoutInMillis();
        assertNotNull(tcpTimeoutInMillis);
    }

    @Test
    public void getTraceDelayInMillis() {
        Integer traceDelayInMillis = unit.getTraceDelayInMillis();
        assertNotNull(traceDelayInMillis);
    }

    @Test
    public void getReportUrl() {
        String reportUrl = unit.getReportUrl();
        assertNotNull(reportUrl);
    }
}