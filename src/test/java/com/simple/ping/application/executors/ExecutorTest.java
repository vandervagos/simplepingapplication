package com.simple.ping.application.executors;

import com.simple.ping.application.services.ConnectionService;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;

import static org.junit.Assert.assertEquals;

public class ExecutorTest {

    private Executor unit;
    private SingleRunnableFactory singleRunnableFactory = new SingleRunnableFactoryImpl();
    private ScheduledExecutorService executorService = Executors.newScheduledThreadPool(10);

    /**
     * Total time |------------------------------450ms--------------------------------|
     * Host 1     |1st at 0ms|2nd at 100ms|3rd at 200ms|4th at 300ms|5th at 400ms|
     * Host 2     |1st at 0ms|2nd at 100ms|3rd at 200ms|4th at 300ms|5th at 400ms|
     */
    @Test
    public void shouldExecuteConnectionServiceForEachHost() throws InterruptedException {
        //GIVEN
        String[] hosts = {"host1", "host2"};
        TestConnectionService testConnectionService = new TestConnectionService(100, hosts);
        List<ConnectionService> connectionServices = Collections.singletonList(testConnectionService);
        unit = new Executor(singleRunnableFactory, connectionServices, hosts, executorService);

        //WHEN
        unit.runConnectionChecks();
        TimeUnit.MILLISECONDS.sleep(450);
        executorService.shutdownNow();

        //THEN
        for (String host : hosts) {
            assertEquals(5, testConnectionService.getExecutions(host));
        }
    }

    /**
     * Total time |-----------------------------450ms------------------------------|
     * Service 1     |1st at 0s|2nd at 100ms|3rd at 200ms|4th at 300ms|5th at 400ms|
     * Service 2     |1st at 0s             |2nd at 200ms             |3rd at 400ms
     * Service 3     |1st at 0s                          |2nd at 300ms
     */
    @Test
    public void shouldExecuteConnectionServiceForEachService() throws InterruptedException {
        //GIVEN
        String[] hosts = {"host"};
        TestConnectionService testConnectionService1 = new TestConnectionService(100, hosts);
        TestConnectionService testConnectionService2 = new TestConnectionService(200, hosts);
        TestConnectionService testConnectionService3 = new TestConnectionService(300, hosts);
        List<ConnectionService> connectionServices = Arrays.asList(testConnectionService1, testConnectionService2, testConnectionService3);
        unit = new Executor(singleRunnableFactory, connectionServices, hosts, executorService);

        //WHEN
        unit.runConnectionChecks();
        TimeUnit.MILLISECONDS.sleep(450);
        executorService.shutdownNow();

        //THEN
        assertEquals(5, testConnectionService1.getExecutions(hosts[0]));
        assertEquals(3, testConnectionService2.getExecutions(hosts[0]));
        assertEquals(2, testConnectionService3.getExecutions(hosts[0]));
    }

    /**
     * Total time |---------------------------450ms-----------------------------|
     * Periods    |1st at 0s|2nd at 100ms|3rd at 200ms|4th at 300ms|5th at 400ms|
     * Execution  |---1st 130ms--|     |---2nd 130ms--|     |---3rd 130ms--|
     */
    @Test
    public void shouldWaitUntilPreviousIsComplete() throws InterruptedException {
        //GIVEN
        String[] hosts = {"host"};
        TestConnectionService testConnectionService = new TestConnectionService(100, hosts);
        testConnectionService.setFunction(o -> {
            try {
                TimeUnit.MILLISECONDS.sleep(130);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        });
        List<ConnectionService> connectionServices = Collections.singletonList(testConnectionService);
        unit = new Executor(singleRunnableFactory, connectionServices, hosts, executorService);

        //WHEN
        unit.runConnectionChecks();
        TimeUnit.MILLISECONDS.sleep(450);
        executorService.shutdownNow();

        //THEN
        assertEquals(3, testConnectionService.getExecutions(hosts[0]));

    }

    @Test
    public void shouldStopRunningIfExceptionOccurs() throws InterruptedException {
        //GIVEN
        String[] hosts = {"host"};
        TestConnectionService testConnectionService = new TestConnectionService(20, hosts);
        List<ConnectionService> connectionServices = Collections.singletonList(testConnectionService);
        ExceptionFactory exceptionFactory = new ExceptionFactory();
        unit = new Executor(exceptionFactory, connectionServices, hosts, executorService);

        //WHEN
        unit.runConnectionChecks();
        TimeUnit.MILLISECONDS.sleep(100);
        executorService.shutdownNow();

        //THEN
        assertEquals(1, exceptionFactory.getNumberOfThreads());
        assertEquals(1,
                     exceptionFactory.getFirstRunnable()
                                     .getExecutions());

    }

    public class ExceptionFactory implements SingleRunnableFactory {

        List<ExceptionSingleRunnable> threads = new ArrayList<>();

        @Override
        public SingleRunnable singleRunnable(Thread connectionServiceThread) {
            ExceptionSingleRunnable exceptionSingleRunnable = new ExceptionSingleRunnable(connectionServiceThread);
            threads.add(exceptionSingleRunnable);
            return exceptionSingleRunnable;
        }

        public int getNumberOfThreads() {
            return threads.size();
        }

        public ExceptionSingleRunnable getFirstRunnable() {
            return threads.get(0);
        }
    }

    public class ExceptionSingleRunnable extends SingleRunnable {

        private AtomicInteger counter = new AtomicInteger();

        protected ExceptionSingleRunnable(Runnable connectionService) {
            super(connectionService);
        }

        @Override
        public void run() {
            counter.incrementAndGet();
            throw new RuntimeException();
        }

        public int getExecutions() {
            return counter.get();
        }
    }

    public class TestConnectionService implements ConnectionService {

        private final Map<String, AtomicInteger> map;
        private final Integer delay;
        private Function function;

        public TestConnectionService(Integer delay, String[] hosts) {
            this.map = new HashMap<>();
            for (String host : hosts) {
                map.put(host, new AtomicInteger());
            }
            this.delay = delay;
        }

        @Override
        public void run(String host) {
            map.get(host)
               .incrementAndGet();
            if (function != null) {
                function.apply(null);
            }
        }

        @Override
        public int getDelay() {
            return delay;
        }

        public int getExecutions(String host) {
            return map.get(host)
                      .get();
        }

        public void setFunction(Function function) {
            this.function = function;
        }
    }
}