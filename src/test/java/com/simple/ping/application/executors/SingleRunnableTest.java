package com.simple.ping.application.executors;

import org.junit.Test;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.assertEquals;

public class SingleRunnableTest {

    private SingleRunnable unit;

    @Test
    public void shouldExecuteOnlyOneAtATime() throws InterruptedException {
        //WHEN
        AtomicInteger counter = new AtomicInteger(0);

        //Increments atomic counter and sleep for long time
        unit = new SingleRunnable(() -> {
            try {
                counter.incrementAndGet();
                TimeUnit.MINUTES.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        //WHEN
        Thread thread1 = new Thread(unit);
        Thread thread2 = new Thread(unit);
        thread1.start();
        //Make sure thread one in in "sleep mode" before start thread 2
        TimeUnit.MILLISECONDS.sleep(100);
        thread2.start();
        //Make sure there is enough time for thread 2 to execute the increment
        //if our code is not controlling properly the single run
        TimeUnit.MILLISECONDS.sleep(100);

        //THEN
        //Interrupt threads and check results
        thread1.interrupt();
        thread2.interrupt();

        assertEquals(1, counter.get());
    }

    @Test
    public void shouldContinueEvenIfExceptionOccurs() throws InterruptedException {
        //WHEN
        AtomicInteger counter = new AtomicInteger(0);

        //Increments atomic counter and sleep for long time
        unit = new SingleRunnable(() -> {
            if (counter.incrementAndGet() == 1) {
                throw new RuntimeException();
            }
        });

        //WHEN
        new Thread(unit).start();
        new Thread(unit).start();

        //Wait for threads to complete
        TimeUnit.MILLISECONDS.sleep(100);

        //THEN
        assertEquals(2, counter.get());
    }
}