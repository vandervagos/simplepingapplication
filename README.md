Based on the  functionality I had to make some assumptions

- The application was build on Java 1.8
- The only dependency that I have used is junit, no other framework, not even Mockito.
- For Ping and Trace commands I am executing native OS commands based on the OS (Win/Unix)
- By "store the last result" I made the assumption that this should be stored in a file in order to re-start the application if needed
- I assumed that a GET HTTP request is what was needed for Ping with TCP/IP
- It is not clear if the timeout for the TCP/IP ping is connection or response timeout, so I assumed the same timeout should be used for both
- The result format is very vague so I assumed that it will be a text field that contains all the information needed (host, url, response time etc)
- It is not clear if the TCP/IP ping url is different from the original host, so I used the host addresses.
- The application can be configured by having a config.properties file in the same directory with jar file.
- In the config.properties you can define the log file, otherwise it is using the default.
- Application needs internet access in order for tests to pass successfully because there is a dependency on postman echo.
